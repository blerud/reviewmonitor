from jinja2 import Environment, FileSystemLoader

import csv
import datetime

def create_html():
    env = Environment(loader=FileSystemLoader('templates'))
    template = env.get_template('template.html')

    infilename = datetime.datetime.now().strftime('%m%d%Y')+'.csv'
    outfilename = datetime.datetime.now().strftime('%m%d%Y')+'.html'
    keywordsfilename = 'keywords.txt'
    infile = open(infilename, 'r')
    outfile = open(outfilename, 'w')
    keywordsfile = open(keywordsfilename, 'r')
    products = []
    productsreader = csv.DictReader(infile)
    for row in productsreader:
        if row['product_name'] == 'product_name':
            continue
        if row['response_from_tplink'] == 'True':
            continue
        pname = ' '.join(row['product_name'].split(' ')[:3])
        if not pname in products:
            products.append(pname)
    infile.close()
    infile = open(infilename, 'r')
    reader = csv.DictReader(infile)
    keywords = keywordsfile.read().splitlines()
    keywordsfile.close()
    reviewdata = []
    for row in reader:
        if len(row) != 10:
            continue
        if row['product_name'] == 'product_name' or row['response_from_tplink'] == 'True':
            continue
        line = []
        line.append(row['review_title'])
        line.append(row['review_url'])
        line.append(row['review_text'])
        line.append(row['review_rating'])
        line.append(datetime.datetime.strptime(row['review_date'], '%m%d%Y').strftime('%B %d, %Y'))
        reviewkeywords = {}
        for word in row['review_text'].split(' '):
            for kword in keywords:
                if kword in word:
                    if kword in reviewkeywords:
                        reviewkeywords[kword] += 1
                    else:
                        reviewkeywords[kword] = 1
        reviewkeywordstxt = ''
        for word in reviewkeywords:
            reviewkeywordstxt += word + ': ' + str(reviewkeywords[word]) + '<br>'
        line.append(reviewkeywordstxt)
        tagclass = ''
        if datetime.datetime.strptime(row['review_date'], '%m%d%Y') < datetime.datetime.now()-datetime.timedelta(days=2):
            tagclass = 'orange'
        tagclass += ' ' + '_'.join(row['product_name'].split(' ')[:3])
        line.append(tagclass)
        reviewdata.append(line)

    template_output = template.render(tabnames=products, reviewdata=reviewdata)
    with open(outfilename, 'wb') as fh:
        fh.write(template_output)

if __name__ == '__main__':
    create_html()

