import re
import urlparse

def findsite(url):
    u = urlparse.urlparse(url)
    if u.scheme == '':
        return ''
    return u.netloc.split('.')[1]

def find_parameter(parameter_name, text):
    return re.search("[\?&]" + parameter_name + "=[^&]*", text).group(0).split('=')[1]

def getid(url):
    domain = findsite(url)
    if domain == 'amazon':
        #return url.split('/')[5]
        return re.search("/B\w{9}", url).group(0)[1:]
    elif domain == 'newegg':
        return find_parameter('Item', url)
    else:
        return ''
