import matplotlib.pyplot as plt
from matplotlib.dates import DateFormatter

import collections
import csv
import datetime

def makegraph():
    data = {}
    infilename = datetime.datetime.now().strftime('%m%d%Y')+'-allstars.csv'
    infile = open(infilename, 'r')
    reader = csv.DictReader(infile)
    reader.next()
    for row in reader:
        product = row['product_name']
        date = datetime.datetime.strptime(row['review_date'], '%m%d%Y')
        rating = row['review_rating']
        if product in data:
            if date in data[product]:
                if rating in data[product][date]:
                    data[product][date][rating] += 1
                else:
                    data[product][date][rating] = 1
            else:
                data[product][date] = {}
                data[product][date][rating] = 1
        else:
            data[product] = {}
            data[product][date] = {}
            data[product][date][rating] = 1
    infile.close()
    for product in data:
        #dates = [datetime.datetime.strptime(date, '%m%d%Y') for date in data[product].keys()]
        sorteddict = collections.OrderedDict(sorted(data[product].items()))
        dates = sorteddict.keys()
        ratings = [sorteddict[date] for date in dates]
        colors = ['red', 'orange', 'black', 'green', 'blue']
        for i in xrange(1, 6):
            currentratings = []
            for rating in ratings:
                if str(float(i)) in rating.keys():
                    currentratings.append(rating[str(float(i))])
                else:
                    currentratings.append(str(0))
            plt.plot_date(dates, currentratings, fmt='-o', color=colors[i-1], label=str(i)+' star', linewidth=2)
            plt.xticks(dates, rotation=45)
            plt.gcf().axes[0].xaxis.set_major_formatter(DateFormatter('%m/%d'))
            plt.legend()
        plt.show()

if __name__ == '__main__':
    makegraph()
