from scrapy.crawler import CrawlerProcess
from scrapy.utils.project import get_project_settings

from PySide.QtCore import *
from PySide.QtGui import *

from reviewmonitor.spiders import amazonspider, neweggspider

import htmlwriter
import getid
import productlistbuilder

import csv
import datetime
import os
import sys
import webbrowser

settings = get_project_settings()
outfilename = datetime.datetime.now().strftime('%m%d%Y')+'.csv'
settings.set('FEED_URI', outfilename)


class AdvComboBox(QComboBox):
    def __init__(self, parent=None):
        super(AdvComboBox, self).__init__(parent)

        self.setFocusPolicy(Qt.StrongFocus)
        self.setEditable(True)

        # add a filter model to filter matching items
        self.pFilterModel = QSortFilterProxyModel(self)
        self.pFilterModel.setFilterCaseSensitivity(Qt.CaseInsensitive)
        self.pFilterModel.setSourceModel(self.model())

        # add a completer
        self.completer = QCompleter(self)
        #Set the model that the QCompleter uses
        # - in PySide doing this as a separate step worked better
        self.completer.setModel(self.pFilterModel)
        # always show all (filtered) completions
        self.completer.setCompletionMode(QCompleter.UnfilteredPopupCompletion)

        self.setCompleter(self.completer)

        # connect signals
        def filter(text):
            print "Edited: ", text, "type: ", type(text)
            self.pFilterModel.setFilterFixedString(str(text))

        self.lineEdit().textEdited[unicode].connect(filter)
        self.completer.activated.connect(self.on_completer_activated)

    # on selection of an item from the completer, select the corresponding item from combobox
    def on_completer_activated(self, text):
        print "activated"
        if text:
            print "text: ", text
            index = self.findText(str(text))
            print "index: ", index
            self.setCurrentIndex(index)

    # on model change, update the models of the filter and completer as well
    def setModel(self, model):
        super(AdvComboBox, self).setModel(model)
        self.pFilterModel.setSourceModel(model)
        self.completer.setModel(self.pFilterModel)

    # on model column change, update the model column of the filter and completer as well
    def setModelColumn(self, column):
        self.completer.setCompletionColumn(column)
        self.pFilterModel.setFilterKeyColumn(column)
        super(AdvComboBox, self).setModelColumn(column)


class MainWindow(QWidget):
    def __init__(self):
        QWidget.__init__(self)
        layout = QGridLayout()
        layout.setSpacing(10)
        self.setLayout(layout)

        self.combo_box_rating = QComboBox()
        rating_options = ['1', '2', '3', '4', '5']
        self.combo_box_rating.addItems(rating_options)
        self.combo_box_rating.setCurrentIndex(1)
        self.label_rating = QLabel()
        self.label_rating.setText('Enter highest rating (1-5)')
        self.label_date_start = QLabel()
        self.label_date_end = QLabel()
        self.label_date_start.setText('Enter date to start from (mm/dd/yy)')
        self.label_date_end.setText('Enter date to end on (mm/dd/yy)')
        today = datetime.datetime.now()
        self.date_edit_start = QDateEdit()
        self.date_edit_start.setDate(QDate(today.year, today.month, today.day))
        self.date_edit_start.setCalendarPopup(True)
        self.date_edit_end = QDateEdit()
        self.date_edit_end.setDate(QDate(today.year, today.month, today.day))
        self.date_edit_end.setCalendarPopup(True)
        layout.addWidget(self.label_rating)
        layout.addWidget(self.combo_box_rating)
        layout.addWidget(self.label_date_start)
        layout.addWidget(self.date_edit_start)
        layout.addWidget(self.label_date_end)
        layout.addWidget(self.date_edit_end)

        self.product_list = {}
        productfile = open('productlist.csv', 'r')
        productsreader = csv.DictReader(productfile)
        productsreader.next()
        for row in productsreader:
            if not row['product_name'] in self.product_list:
                self.product_list[row['product_name']] = row['product_url']
        self.combo_products = AdvComboBox()
        self.combo_products.addItems(self.product_list.keys())
        self.combo_products.setModelColumn(0)
        self.combo_products.currentIndexChanged.connect(self.add_item)

        self.text_edit_products = QTextEdit()
        self.label_products = QLabel()
        self.label_products.setText('Enter product ids (comma separated)')
        layout.addWidget(self.label_products)
        layout.addWidget(self.combo_products)
        layout.addWidget(self.text_edit_products)

        self.crawl_button = QPushButton()
        self.crawl_button.setText('Crawl')
        self.crawl_button.clicked.connect(self.crawl)
        layout.addWidget(self.crawl_button)
        self.setFixedWidth(500)
        self.show()

        # add an item to the textedit
    def add_item(self):
        selected_item = self.combo_products.currentText()
        item_url = self.product_list[selected_item]
        self.write_text(item_url+',')

        # append text to the textedit
    def write_text(self, text):
        cursor = self.text_edit_products.textCursor()
        cursor.movePosition(QTextCursor.End)
        cursor.insertText(text)
        self.text_edit_products.setTextCursor(cursor)
        self.text_edit_products.ensureCursorVisible()

    def crawl(self):
        self.rating_limit = self.combo_box_rating.currentText().strip()
        self.date_start = self.date_edit_start.date().toString('MMddyyyy')
        self.date_end = self.date_edit_end.date().toString('MMddyyyy')
        self.product_ids = self.text_edit_products.toPlainText().strip()
        product_ids_list = self.product_ids.split(',')
        amazon_ids_list = []
        newegg_ids_list = []
        for i, item in enumerate(product_ids_list):
            if self.get_site(item.strip()) == 0:
                amazon_ids_list.append(item.strip())
            elif self.get_site(item.strip()) == 1:
                newegg_ids_list.append(item.strip())
            else:
                item_id = getid.getid(item.strip())
                if item_id != '':
                    product_ids_list.append(item_id)
        self.amazon_ids = ','.join(amazon_ids_list)
        self.newegg_ids = ','.join(newegg_ids_list)

        runner = CrawlerProcess(settings)
        runner.crawl(amazonspider.AmazonSpider, rating_limit=self.rating_limit, date_start=self.date_start, date_end=self.date_end, product_ids=self.amazon_ids)
        runner.crawl(neweggspider.NeweggSpider, rating_limit=self.rating_limit, date_start=self.date_start, date_end=self.date_end, product_ids=self.newegg_ids)
        runner.start()
        self.display_csv()

    # 0 - amazon
    # 1 - newegg
    def get_site(self, product_id):
        if product_id.startswith('B') and len(product_id) == 10:
            return 0
        elif product_id.startswith('N') and len(product_id) == 15:
            return 1
        return -1

    def display_csv(self):
        htmlwriter.create_html()
        webbrowser.open(datetime.datetime.now().strftime('%m%d%Y')+'.html')

    def build_product_list(self):
        productlistbuilder.build_product_list()


def main():
    if os.path.isfile(outfilename):
        os.remove(outfilename)
    app = QApplication(sys.argv)
    mw = MainWindow()
    app.exec_()

if __name__ == '__main__':
    main()
