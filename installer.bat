@echo off

cscript /nologo wget.js "https://www.python.org/ftp/python/2.7.10/python-2.7.10.amd64.msi" "python-2.7.10.amd64.msi"
REM pywin32 here: http://downloads.sourceforge.net/project/pywin32/pywin32/Build%20219/pywin32-219.win-amd64-py2.7.exe

start /w python-2.7.10.amd64.msi

C:\Python27\Scripts\pip.exe install scrapy pyside jinja2 matplotlib