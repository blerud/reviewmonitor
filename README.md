reviewmonitor
=============

## Requirements:
* Python 2.7
* Scrapy
* win32api
* PySide
* jinja2
* matplotlib

Download and install python 2.7 here: https://www.python.org/downloads/release/python-2710/

Download and install pip here: https://pip.pypa.io/en/latest/installing.html

Download and install win32api here: http://sourceforge.net/projects/pywin32/files/pywin32/Build%20219/

Run the following commands to install Scrapy, PySide and Jinja2:

	pip install scrapy
	pip install pyside
	pip install jinja2

Run the following command to get matplotlib, which is required for the graphing:

    pip install matplotlib

## Usage:

Place keywords you want to find in the `keywords.txt` file.

Run the main python file `launcher.py` with the following command:

	python launcher.py

If `productlist.csv` does not exist, run the following command:

    python productlistbuilder.py

To create the review graph, run the following command:

    python grapher.py
