from scrapy.crawler import CrawlerProcess
from scrapy.utils.project import get_project_settings

from reviewmonitor.spiders import amazonproductlistspider, neweggproductlistspider

settings = get_project_settings()
outfilename = 'productlist.csv'
settings.set('FEED_URI', outfilename)

def build_product_list():
    runner = CrawlerProcess(settings)
    runner.crawl(amazonproductlistspider.AmazonProductListSpider)
    runner.crawl(neweggproductlistspider.NeweggProductListSpider)
    runner.start()

if __name__ == '__main__':
    build_product_list()
