from PySide.QtCore import *
from PySide.QtGui import *

import csv
import datetime
import sys

infilename = datetime.datetime.now().strftime('%m%d%Y')+'.csv'

class CsvOutputWindow(QWidget):
    def __init__(self):
        QWidget.__init__(self)
        layout = QVBoxLayout()
        self.setLayout(layout)

        self.text_edit_outout = QTextEdit()
        layout.addWidget(self.text_edit_outout)
        self.show()

    def display_csv(self):
        with open(infilename) as filein:
            reader = csv.reader(filein)
            for row in reader:
                if len(row) != 9:
                    continue
                response = row[7]
                if response == 'True':
                    continue
                reviewdate = row[3]
                color = 'white'
                if not reviewdate == 'review_date' and datetime.datetime.strptime(reviewdate, '%m%d%Y') < datetime.datetime.now()-datetime.timedelta(days=2):
                    color = 'orange'
                self.write_text(', '.join(row)+'\n\n', color)

    def write_text(self, text, color):
        cursor = self.text_edit_outout.textCursor()
        cursor.movePosition(QTextCursor.End)
        textformat = QTextCharFormat()
        textformat.setBackground(QBrush(QColor(color)))
        cursor.setBlockCharFormat(textformat)
        cursor.insertText(text)
        self.text_edit_outout.setTextCursor(cursor)
        self.text_edit_outout.ensureCursorVisible()


if __name__ == '__main__':
    app = QApplication(sys.argv)
    mw = CsvOutputWindow()
    mw.display_csv()
    app.exec_()
