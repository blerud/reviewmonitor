import scrapy
from reviewmonitor.items import ProductItem
import datetime
import re

class NeweggProductListSpider(scrapy.Spider):
    name = 'neweggproductlistspider'
    site = 'newegg'
    allowed_domains = ['newegg.com']
    start_urls = ['http://www.newegg.com/Product/ProductList.aspx?Submit=ENE&Description=tp-link&Pagesize=90&Page=1']

    def __init__(self, *args, **kwargs):
        super(NeweggProductListSpider, self).__init__(*args, **kwargs)

    def parse(self, response):
        if response.url.startswith('http://www.newegg.com/'):
            for itemdiv in response.xpath('//div[@class="itemCell"]'):
                if itemdiv.xpath('.//a[@class="itemBrand"]').extract_first() is None:
                    continue
                if itemdiv.xpath('.//a[@class="itemBrand"]/@href').extract_first() != 'http://www.newegg.com/TP-LINK/BrandStore/ID-12120':
                    continue
                url = itemdiv.xpath('.//a[@title="View Details"]/@href').extract_first()
                itemname = itemdiv.xpath('.//div/a[@title="View Details"]/span/text()').extract_first()
                imageurl = itemdiv.xpath('.//a[@class="itemImage"]/img/@src').extract_first()

                item = ProductItem()
                item['product_name'] = ' '.join(itemname.split(' ')[1:5]) + ' - newegg'
                item['product_url'] = url
                item['product_image'] = imageurl
                yield item
            # next product page
            currentpage = self.find_parameter('Page', response.url)
            onlastpage = response.xpath('//div[contains(@class, "pagination")]/ul/li/@class')[-1].extract() == 'disabled'
            pageposition = int(re.search("Page=", response.url).end(0))
            if not onlastpage:
                yield scrapy.Request(response.url[0:pageposition] + str(int(currentpage)+1) + response.url[pageposition+1:], callback=self.parse)

    def find_parameter(self, parameter_name, text):
        return re.search("[\?&]" + parameter_name + "=[^&]*", text).group(0).split('=')[1]
