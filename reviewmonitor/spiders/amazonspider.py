import scrapy
from reviewmonitor.items import ReviewmonitorItem
import datetime
import re

class AmazonSpider(scrapy.Spider):
    name = 'amazonspider'
    site = 'amazon'
    allowed_domains = ['amazon.com']

    # date format mmddyyyy
    # product_ids comma separated
    def __init__(self, rating_limit=2, date_start='01011970', date_end = '', product_ids='', *args, **kwargs):
        super(AmazonSpider, self).__init__(*args, **kwargs)
        self.rating_limit = rating_limit
        self.date_start = date_start
        if date_end == '':
            date_end = datetime.datetime.now().strftime('%m%d%Y')
        self.date_end = date_end
        self.product_ids = product_ids

    def start_requests(self):
        ids = self.product_ids.split(',')
        numberstr = ['one', 'two', 'three', 'four', 'five']
        for id in ids:
            for x in xrange(int(self.rating_limit)):
                review_url = 'http://www.amazon.com/product-reviews/' + id + '/ref=cm_cr_pr_show_all?ie=UTF8&showViewpoints=0&sortBy=recent&reviewerType=all_reviews&filterByStar='+numberstr[x]+'_star&pageSize=50&pageNumber=1'
                yield scrapy.Request(review_url, callback=self.parse_requests_page)

    def parse_requests_page(self, response):
        # one review
        for reviewdiv in response.xpath('//div[@class="a-section review"]'):
            review_date_str = reviewdiv.xpath('.//div/span[contains(@class, "review-date")]/text()').extract_first()
            review_date_str_b = ' '.join(review_date_str.split(' ')[1:])
            review_date = self.str_to_date(review_date_str_b)
            # skips dates after date_end
            if datetime.datetime.strptime(review_date, '%m%d%Y') > datetime.datetime.strptime(self.date_end, '%m%d%Y'):
                continue
            # stop after date_start
            if datetime.datetime.strptime(review_date, '%m%d%Y') < datetime.datetime.strptime(self.date_start, '%m%d%Y'):
                return
            url = reviewdiv.xpath('.//div/a[contains(@class, "review-title")]/@href').extract_first()
            yield scrapy.Request('http://amazon.com'+url, callback=self.parse_individual_request)
        # next review page
        for url in response.xpath('//li[@class="a-last"]/a/@href').extract():
            yield scrapy.Request('http://amazon.com'+url, callback=self.parse_requests_page)

    def parse_individual_request(self, response):
        # get info
        item = ReviewmonitorItem()
        item['review_site'] = self.site
        item['review_url'] = response.url
        item['review_rating'] = response.xpath('//td/div/div/span/img/@alt').extract_first().split(' ')[0]
        date_str = response.xpath('//td/div/div/nobr/text()').extract_first()
        item['review_date'] = self.str_to_date(date_str)
        item['review_title'] = response.xpath('//td/div/div/b/text()').extract_first()
        item['review_text'] = response.xpath('//div[@class="reviewText"]/text()').extract_first()
        item['product_name'] = response.xpath('//div[@class="tiny"]/b/text()').extract_first()
        item['product_url'] = response.xpath('//div[@class="crDescription"]/div/a/@href').extract_first()
        found_comment = False
        for comment in response.xpath('//div[@class="postBody"]'):
            name = comment.xpath('.//div[@class="postFrom"]/a/text()').extract_first()
            if name == 'TP-LINK Support Force':
                found_comment = True
                item['response_from_tplink'] = True
                response_date_str = "".join(comment.xpath('.//div[@class="postHeader"]/text()').extract())
                #response_date_str = " ".join(filter(None, re.split('[\s]', response_date_str)))
                response_date_str = re.findall('\w+ \d+, \d+', response_date_str)[0]
                item['response_date'] = self.str_to_date2(response_date_str)
                break
        if not found_comment:
            item['response_from_tplink'] = False
            item['response_date'] = ""
        yield item

    def str_to_date(self, date_str):
        return datetime.datetime.strptime(date_str, '%B %d, %Y').strftime('%m%d%Y')

    def str_to_date2(self, date_str):
        return datetime.datetime.strptime(date_str, '%b %d, %Y').strftime('%m%d%Y')
