import scrapy
from reviewmonitor.items import ReviewmonitorItem
import datetime
import re

class NeweggSpider(scrapy.Spider):
    name = 'neweggspider'
    site = 'newegg'
    allowed_domains = ['newegg.com']

    # date format mmddyyyy
    # product_ids comma separated
    def __init__(self, rating_limit=2, date_start='01011970', date_end='', product_ids='', *args, **kwargs):
        super(NeweggSpider, self).__init__(*args, **kwargs)
        self.rating_limit = rating_limit
        self.date_start = date_start
        if date_end == '':
            date_end = datetime.datetime.now().strftime('%m%d%Y')
        self.date_end = date_end
        self.product_ids = product_ids

    def start_requests(self):
        ids = self.product_ids.split(',')
        for id in ids:
            for x in xrange(int(self.rating_limit)):
                review_url = 'http://www.newegg.com/Product/Product.aspx?Item=' + id + '&SortField=0&SummaryType=0&PageSize=100&SelectedRating=' + str(x+1) + '&VideoOnlyMark=False&IsFeedbackTab=true&Page=1#scrollFullInfo'
                yield scrapy.Request(review_url, callback=self.parse_requests_page)

    def parse_requests_page(self, response):
        # one review
        for reviewtr in response.xpath('//table[@class="grpReviews"]/tbody/tr'):
            review_date_str = reviewtr.xpath('.//th[@class="reviewer"]/ul/li')[1].extract()
            review_date_str = re.findall('\d+/\d+/\d+', review_date_str)[0]
            review_date = self.str_to_date(review_date_str.split(' ')[0])
            # skips dates after date_end
            if datetime.datetime.strptime(review_date, '%m%d%Y') > datetime.datetime.strptime(self.date_end, '%m%d%Y'):
                continue
            # stop after date_start
            if datetime.datetime.strptime(review_date, '%m%d%Y') < datetime.datetime.strptime(self.date_start, '%m%d%Y'):
                return
            idstr = reviewtr.xpath('.//div[@class="voteMessage"]/@id').extract_first()
            review_id = idstr[3:]
            yield scrapy.Request('http://www.newegg.com/Product/SingleProductReview.aspx?reviewid='+review_id, callback=self.parse_individual_request)
        totalpages_str = response.xpath('//div[@class="pageTotal"]/text()').extract_first()
        if totalpages_str is None:
            return
        totalpages = totalpages_str.split(' ')[1]
        currentpage = self.find_parameter('Page', response.url)
        pageposition = int(re.search("Page=", response.url).end(0))
        if int(currentpage) < int(totalpages):
            yield scrapy.Request(response.url[0:pageposition] + str(int(currentpage)+1) + response.url[pageposition+1:], callback=self.parse_requests_page)

    def parse_individual_request(self, response):
        # get info
        item = ReviewmonitorItem()
        item['review_site'] = self.site
        item['review_url'] = response.url
        item['review_rating'] = response.xpath('//span[@class="itmRating"]/img/@alt').extract_first().split(' ')[0]
        date_str = response.xpath('//span[contains(@class, "review-date")]/strong/text()').extract_first()
        item['review_date'] = self.str_to_date(date_str)
        item['review_title'] = response.xpath('//div[@class="review-title"]/h2/text()').extract_first().strip()
        review_text = ''
        for reviewtext in response.xpath('//div[contains(@class, "review-pros")]'):
            subtitle = reviewtext.xpath('.//h3/text()').extract_first().strip()
            msg = reviewtext.xpath('.//p/text()').extract_first().strip()
            review_text += subtitle + " " + msg + "\n" 
        item['review_text'] = review_text[:-1]
        item['product_name'] = response.xpath('//div[@class="review-title"]/p/a/text()').extract_first().strip()
        item['product_url'] = response.xpath('//div[@class="review-title"]/p/a/@href').extract_first().strip()
        item['response_from_tplink'] = response.xpath('//blockquote[@class="manufacturer-response"]').extract_first() is None
        item['response_date'] = ''
        yield item

    def str_to_date(self, date_str):
        return datetime.datetime.strptime(date_str, '%m/%d/%Y').strftime('%m%d%Y')

    def find_parameter(self, parameter_name, text):
        return re.search("[\?&]" + parameter_name + "=[^&]*", text).group(0).split('=')[1]
