import scrapy
from reviewmonitor.items import ProductItem
import datetime
import re

class AmazonProductListSpider(scrapy.Spider):
    name = 'amazonproductlistspider'
    site = 'amazon'
    allowed_domains = ['amazon.com']
    start_urls = ['http://www.amazon.com/s/&field-keywords=tp-link']

    def __init__(self, *args, **kwargs):
        super(AmazonProductListSpider, self).__init__(*args, **kwargs)

    def parse(self, response):
        if response.url.startswith('http://www.amazon.com/s'):
            for itemdiv in response.xpath('//li[@class="s-result-item"]'):
                smalltext = ' '.join(itemdiv.xpath('.//div[@class="a-row a-spacing-none"]/span[@class="a-size-small a-color-secondary"]/text()').extract())
                if smalltext == '':
                    continue
                company = filter(None, smalltext.split(' '))[1]
                if company.upper() != "TP-LINK":
                    continue
                url = itemdiv.xpath('.//div/a[contains(@class, "s-access-detail-page")]/@href').extract_first()
                itemname = itemdiv.xpath('.//div/a[contains(@class, "s-access-detail-page")]/@title').extract_first()
                imageurl = itemdiv.xpath('.//img[contains(@class, "s-access-image")]/@src').extract_first()

                item = ProductItem()
                item['product_name'] = ' '.join(itemname.split(' ')[1:5]) + ' - amazon'
                item['product_url'] = url
                item['product_image'] = imageurl
                yield item
            # next product page
            for url in response.xpath('//a[@id="pagnNextLink"]/@href').extract():
                yield scrapy.Request('http://amazon.com'+url, callback=self.parse)
