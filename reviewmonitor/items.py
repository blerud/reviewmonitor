# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class ReviewmonitorItem(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    review_site = scrapy.Field()
    review_url = scrapy.Field()
    review_rating = scrapy.Field()
    review_date = scrapy.Field()
    review_title = scrapy.Field()
    review_text = scrapy.Field()
    product_name  = scrapy.Field()
    product_url = scrapy.Field()
    response_from_tplink = scrapy.Field()
    response_date = scrapy.Field()

class ProductItem(scrapy.Item):
    product_name = scrapy.Field()
    product_url = scrapy.Field()
    product_image = scrapy.Field()
