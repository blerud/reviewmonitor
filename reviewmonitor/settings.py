# -*- coding: utf-8 -*-

# Scrapy settings for reviewmonitor project
#
# For simplicity, this file contains only the most important settings by
# default. All the other settings are documented here:
#
#     http://doc.scrapy.org/en/latest/topics/settings.html
#

BOT_NAME = 'reviewmonitor'

SPIDER_MODULES = ['reviewmonitor.spiders']
NEWSPIDER_MODULE = 'reviewmonitor.spiders'

LOG_ENABLED=True
LOG_LEVEL='DEBUG'

# Crawl responsibly by identifying yourself (and your website) on the user-agent
#USER_AGENT = 'reviewmonitor (+http://www.yourdomain.com)'

FEED_FORMAT = 'csv'
#FEED_EXPORT_FIELDS = ['review_site', 'review_url', 'review_rating', 'review_date', 'review_text', 'product_name', 'product_url', 'response_from_tplink', 'response_date']
